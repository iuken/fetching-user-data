package com.aziattsev.alexander.service;

import com.aziattsev.alexander.entity.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

/**
 * Custom deserializer class. Used by Jackson to deserialize a JSON to User entities.
 *
 * Please see the {@link com.aziattsev.alexander.entity.User} class
 */

public class UserDeserializer extends JsonDeserializer<User> {

    /**
     * Custom deserializer class. Used by Jackson to deserialize a JSON to User entities:
     *
     * @return {@link com.aziattsev.alexander.entity.User} entity
     * @throws IOException
     */

    @Override
    public User deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.readValueAsTree();
        JsonNode data = node.get("data");

        long id = data.get("id").longValue();
        String email = data.get("email").asText();
        String firstName = data.get("first_name").asText();
        String lastName = data.get("last_name").asText();
        String avatarUrl = data.get("avatar").asText();

        return new User(id, email, firstName, lastName, avatarUrl);
    }
}
