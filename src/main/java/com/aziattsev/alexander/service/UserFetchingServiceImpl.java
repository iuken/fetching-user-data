package com.aziattsev.alexander.service;

import com.aziattsev.alexander.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UserFetchingServiceImpl implements UserFetchingService {

    private String urlPrefix = "https://reqres.in/api/users/";

    private static void cleanup(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public User fetchById(long userId) throws IllegalArgumentException {
        ObjectMapper objectMapper = new ObjectMapper();
        URL jsonUrl;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        User user;
        try {
            jsonUrl = new URL(urlPrefix + userId);
            connection = (HttpURLConnection) jsonUrl.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.connect();
            inputStream = connection.getInputStream();
            user = objectMapper.readValue(inputStream, User.class);
        } catch (IOException e) {
            throw new IllegalArgumentException("User not found!");
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            cleanup(inputStream);
        }
        return user;
    }
}
