package com.aziattsev.alexander.service;

import com.aziattsev.alexander.entity.User;

/**
 * Service for fetching user data from an external service
 */
public interface UserFetchingService {

    /**
     * Fetching {@link com.aziattsev.alexander.entity.User} by id.
     *
     * @param userId user id
     * @return {@link com.aziattsev.alexander.entity.User} entity
     * @throws IllegalArgumentException if the user with the specified id is not found
     */
    User fetchById(long userId) throws IllegalArgumentException;


}