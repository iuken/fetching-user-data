package com.aziattsev.alexander;

import com.aziattsev.alexander.entity.User;
import com.aziattsev.alexander.service.UserFetchingService;
import com.aziattsev.alexander.service.UserFetchingServiceImpl;

public class Main {
    public static void main(String[] args) {
        long id = Long.parseLong(args[0]);
        UserFetchingService userFetchingService = new UserFetchingServiceImpl();
        User user = userFetchingService.fetchById(id);
        System.out.println(user.getFirstName() + " " + user.getLastName());
    }
}
