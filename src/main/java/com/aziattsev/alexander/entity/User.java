package com.aziattsev.alexander.entity;

import com.aziattsev.alexander.service.UserDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * The User class is the main entity. User class is using to provide user information as an object
 *
 */

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = UserDeserializer.class)
public class User {
    /**
     * User id.
     */
    private long id;

    /**
     * User email address
     */
    private String email;

    /**
     * User first name
     */
    private String firstName;

    /**
     * User last name
     */
    private String lastName;

    /**
     * User avatar URL
     */
    private String avatarUrl;

}
